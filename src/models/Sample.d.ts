/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export type Name = string;
export type Comments = null | string;
/**
 * Location in container
 */
export type Location = number;
export type Containerid = number;
/**
 * Number of sub samples
 */
export type Subsamples = number;
/**
 * Number of data collections
 */
export type Datacollections = number;
/**
 * Types of data collections
 */
export type Types = null | string[];
/**
 * Whether this sample is queued for data collection
 */
export type Queued = boolean;
/**
 * Number of successful strategies
 */
export type Strategies = number;
/**
 * Number of successful auto-integrations
 */
export type Autointegrations = number;
/**
 * Highest integration resolution
 */
export type Integratedresolution = null | number;
/**
 * The associated proposal
 */
export type Proposal = string;
export type Blsampleid = number;
export type Crystal = SampleCrystal;
export type CellA = null | number;
export type CellB = null | number;
export type CellC = null | number;
export type CellAlpha = null | number;
export type CellBeta = null | number;
export type CellGamma = null | number;
export type Protein = SampleProtein;
export type Proposalid = string;
export type Name1 = string;
export type Acronym = string;
export type Crystalid = number;
export type Protein1 = number;
export type Container = SampleContainer;
export type Code = string;
/**
 * Position in sample change
 */
export type SampleChangerLocation = string;
/**
 * Beamline if container is assigned
 */
export type BeamlineLocation = string;

export interface Sample {
  name: Name;
  comments?: Comments;
  location?: Location;
  containerId?: Containerid;
  _metadata?: SampleMetaData;
  blSampleId: Blsampleid;
  Crystal: Crystal;
  Container?: Container;
}
export interface SampleMetaData {
  subsamples: Subsamples;
  datacollections: Datacollections;
  types?: Types;
  queued?: Queued;
  strategies?: Strategies;
  autoIntegrations?: Autointegrations;
  integratedResolution?: Integratedresolution;
  proposal?: Proposal;
}
export interface SampleCrystal {
  cell_a?: CellA;
  cell_b?: CellB;
  cell_c?: CellC;
  cell_alpha?: CellAlpha;
  cell_beta?: CellBeta;
  cell_gamma?: CellGamma;
  Protein: Protein;
  crystalId: Crystalid;
  proteinId: Protein1;
}
export interface SampleProtein {
  proposalId: Proposalid;
  name: Name1;
  acronym: Acronym;
}
export interface SampleContainer {
  code: Code;
  sampleChangerLocation?: SampleChangerLocation;
  beamlineLocation?: BeamlineLocation;
}

type Constructor<T = {}> = new (...args: any[]) => T;
export function withSample<TBase extends Constructor>(Base: TBase) {
  return class WithSample extends Base {
    name: Name;
    comments?: Comments;
    location?: Location;
    containerId?: Containerid;
    _metadata?: SampleMetaData;
    blSampleId: Blsampleid;
    Crystal: Crystal;
    Container?: Container;
  };
}
export function withSampleMetaData<TBase extends Constructor>(Base: TBase) {
  return class WithSampleMetaData extends Base {
    subsamples: Subsamples;
    datacollections: Datacollections;
    types?: Types;
    queued?: Queued;
    strategies?: Strategies;
    autoIntegrations?: Autointegrations;
    integratedResolution?: Integratedresolution;
    proposal?: Proposal;
  };
}
export function withSampleCrystal<TBase extends Constructor>(Base: TBase) {
  return class WithSampleCrystal extends Base {
    cell_a?: CellA;
    cell_b?: CellB;
    cell_c?: CellC;
    cell_alpha?: CellAlpha;
    cell_beta?: CellBeta;
    cell_gamma?: CellGamma;
    Protein: Protein;
    crystalId: Crystalid;
    proteinId: Protein1;
  };
}
export function withSampleProtein<TBase extends Constructor>(Base: TBase) {
  return class WithSampleProtein extends Base {
    proposalId: Proposalid;
    name: Name1;
    acronym: Acronym;
  };
}
export function withSampleContainer<TBase extends Constructor>(Base: TBase) {
  return class WithSampleContainer extends Base {
    code: Code;
    sampleChangerLocation?: SampleChangerLocation;
    beamlineLocation?: BeamlineLocation;
  };
}
